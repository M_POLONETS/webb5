<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

function have_no_errors($error){
  foreach ($error as $key) {
    if($key){return false;}
  }
  return true;
};

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();
  $errors_value = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    // Выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['yob'] = !empty($_COOKIE['yob_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['sp'] = !empty($_COOKIE['sp_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);

  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['name']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('name_error', '', 100000);
    // Выводим сообщение.
    $errors_value['name'] = empty($_COOKIE['name_error_value']) ? '' : $_COOKIE['name_error_value'];
    setcookie('name_error_value', '', 100000);
    $messages[] = '<div class="error">Укажите имя. При заполнении поля используйте только символы киррилицы либо латиницы.</div>';
  }
  if ($errors['email']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('email_error', '', 100000);
    // Выводим сообщение.
    $errors_value['email'] = empty($_COOKIE['email_error_value']) ? '' : $_COOKIE['email_error_value'];
    setcookie('email_error_value', '', 100000);
    $messages[] = '<div class="error">Укажите email. Убедитесь, что поле заполнено в соответствии с шаблоном.</div>';
  }
  if ($errors['yob']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('yob_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Укажите год рождения.</div>';
  }
  if ($errors['sex']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('sex_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Выберете пол.</div>';
  }
  if ($errors['sp']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('sp_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Выберете суперспособности/</div>';
  }
  if ($errors['limbs']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('limbs_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Укажите количество конечностей.</div>';
  }
  if ($errors['checkbox']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('checkbox_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Галочку поставьте!</div>';
  }

  // Складываем предыдущие значения полей в массив, если есть.
  // При этом санитизуем все данные для безопасного отображения в браузере.
  $values = array();
  $values['name'] = empty($_COOKIE['name_value']) ? '' : strip_tags($_COOKIE['name_value']);
  $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
  $values['yob'] = empty($_COOKIE['yob_value']) ? '' : strip_tags($_COOKIE['yob_value']);
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
  $values['sp'] = empty($_COOKIE['sp_value']) ? '' : unserialize($_COOKIE['sp_value']);
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : strip_tags($_COOKIE['limbs_value']);
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : strip_tags($_COOKIE['biography_value']);
  $values['sp0']=empty($values['sp'][0]) ? '' :$values['sp'][0]; $values['sp1']=empty($values['sp'][1]) ? '' :$values['sp'][1]; $values['sp2']=empty($values['sp'][2]) ? '' :$values['sp'][2];


  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  // массив $errors всегда будет непустым(там всегда есть либо true, либо false) => надо создать собственную функцию, проверяющую наличие ошибок.
  if (have_no_errors($errors) && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // Загрузка данных юзера из БД
    $user = 'u20984';
    $pass = '7942068';
    $id = $_SESSION['uid'];
    $db = new PDO('mysql:host=localhost;dbname=7942068', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    try {
      $stmt = $db->prepare("SELECT `name`, `email`, `year_of_birth`, `gender`, `num_of_limbs`, `biography` FROM `user` WHERE id = ?");
      $stmt->execute(array($id));
      $result_user = $stmt->fetchAll();
      $values['name'] = strip_tags($result_user[0][0]);
      $values['email'] = strip_tags($result_user[0][1]);
      $values['yob'] = strip_tags($result_user[0][2]);
      $values['sex'] = strip_tags($result_user[0][3]);
      $values['limbs'] = strip_tags($result_user[0][4]);
      $values['biography'] = strip_tags($result_user[0][5]);
      $stmt = $db->prepare("SELECT `sp_name` FROM `super_power_map` WHERE sp_id = ANY(SELECT `sp_id` FROM `super_power` WHERE id = ?)");
      $stmt->execute(array($id));
      $result_user = $stmt->fetchAll();
      $values['sp0']=""; $values['sp1']=""; $values['sp2']="";
      for ($i=0; $i < count($result_user); $i++) { 
        $values["sp$i"]=strip_tags($result_user[$i][0]);
      }
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
    }
    printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
  }

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  $pattern = '/^([А-Я]{1}[а-яё]{1,23}|[A-Z]{1}[a-z]{1,23})$/u';
  if (preg_match($pattern, $_POST['name']) && !empty($_POST['name'])) {
   // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('name_value', $_POST['name'], time() + 12 * 30 * 24 * 60 * 60); 
  }
  else {
   // Выдаем куку на день с флажком об ошибке в поле name.
    setcookie('name_error_value', $_POST['name'], time() + 24 * 60 * 60);
    setcookie('name_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  $pattern = '/\w+@[a-z]+\.[a-z]+/';
  if (preg_match($pattern, $_POST['email']) && !empty($_POST['name'])) {
   setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
  }
  else {
    setcookie('email_error_value', $_POST['email'], time() + 24 * 60 * 60);
   setcookie('email_error', '1', time() + 24 * 60 * 60);
   $errors = TRUE;
  }

  if (empty($_POST['sex'])) {
    setcookie('sex_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('sex_value', $_POST['sex'], time() + 12 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['yob'])) {
    setcookie('yob_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('yob_value', $_POST['yob'], time() + 12 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['limbs'])) {
    setcookie('limbs_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('limbs_value', $_POST['limbs'], time() + 12 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['superpowers'])) {
    setcookie('sp_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('sp_value', serialize($_POST['superpowers']), time() + 12 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['limbs'])) {
    // Выдаем куку на день с флажком об ошибке в поле name.
    setcookie('limbs_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
      // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('limbs_value', $_POST['limbs'], time() + 12 * 30 * 24 * 60 * 60);
  }

  if (empty($_POST['checkbox'])) {
    // Выдаем куку на день с флажком об ошибке в поле name.
    setcookie('checkbox_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }

  if (!empty($_POST['biography'])) {
    setcookie('biography_error', $_POST['biography'], time() + 12 * 30 * 24 * 60 * 60);
    $errors = FALSE;
  }
  
  if ($errors) {
  // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
  header('Location: index.php');
  exit();
  }
  else {
      // Удаляем Cookies с признаками ошибок.
      setcookie('name_error', '', 100000);
      setcookie('email_error', '', 100000);
      setcookie('yob_error', '', 100000);
      setcookie('sex_error', '', 100000);
      setcookie('sp_error', '', 100000);
      setcookie('limbs_error', '', 100000);
      setcookie('checkbox_error', '', 100000);
  }

  $name = $_POST['name'];
  $email = $_POST['email'];
  $year_of_birth = $_POST['yob'];
  $gender = $_POST['sex'];
  $num_of_limbs = $_POST['limbs'];
  $sp = $_POST['superpowers'];
  $biography = $_POST['biography'];

  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {

    // TODO: перезаписать данные в БД новыми данными,
    // кроме логина и пароля.
    $user = 'u20984';
    $pass = '7942068';
    $db = new PDO('mysql:host=localhost;dbname=u20984', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    try {
      $stmt = $db->prepare("UPDATE `user` SET name = ?, email = ?, year_of_birth = ?, gender = ?, num_of_limbs = ?, biography = ? WHERE id = ?");
      $stmt -> execute(array($name, $email, $year_of_birth, $gender, $num_of_limbs, $biography, $_SESSION['uid']));

      $stmt = $db->prepare("DELETE FROM `super_power` WHERE id = ?");
      $stmt -> execute(array($_SESSION['uid']));

      $sp_id = array();

      for($i=0; $i<count($sp); $i++){
        $stmt = $db->prepare("SELECT `sp_id` FROM `super_power_map` WHERE sp_name = ?");
        $stmt ->execute(array($sp[$i]));
        $sp_id[$i] = $stmt->fetchColumn();
      }

      for ($i=0; $i<count($sp); $i++) { 
        $stmt = $db->prepare("INSERT INTO `super_power` SET id = ?, sp_id = ?");
        $stmt -> execute(array($_SESSION['uid'], $sp_id[$i]));
      }

    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    }
  }
  else {
    // Генерируем уникальный логин и пароль.
    // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().

    $user = 'u20984';
    $pass = '7942068';
    $db = new PDO('mysql:host=localhost;dbname=u20984', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    $stmt = $db->prepare("SELECT max(`id`) FROM `user`");
    $stmt->execute();
    $result_user = $stmt->fetch(PDO::FETCH_NUM);

    $login = $result_user[0]+1 . bin2hex(openssl_random_pseudo_bytes(3));
    $pass = bin2hex(openssl_random_pseudo_bytes(4));
    $h_pass = password_hash($pass, PASSWORD_BCRYPT, ["cost" => 10]);
    // Сохраняем в Cookies.
    setcookie('login', $login);
    setcookie('pass', $pass);

    // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
    try {
      $stmt = $db->prepare("INSERT INTO `user` SET name = ?, email = ?, year_of_birth = ?, gender = ?, num_of_limbs = ?, biography = ?, login = ?, pass = ?");
      $stmt -> execute(array($name, $email, $year_of_birth, $gender, $num_of_limbs, $biography, $login, $h_pass));
      $stmt = $db->prepare("SELECT `id` FROM `user` WHERE name = ? AND email = ?"); //м/б такое условие WHERE login = ? а потом $stmt->execute(array($login))
      $stmt->execute(array($name, $email));
      $result_user = $stmt->fetchAll();
      $sp_id = array();
      for($i=0; $i<count($sp); $i++)
      {
        $stmt = $db->prepare("SELECT `sp_id` FROM `super_power_map` WHERE sp_name = ?");
        $stmt ->execute(array($sp[$i]));
        $sp_id[$i] = $stmt->fetchColumn();
      }
      
      for($i=0; $i<count($sp); $i++)
      {
        $stmt = $db->prepare("INSERT INTO `super_power` SET id = ?, sp_id = ?");
        $stmt ->execute(array($result_user[0]["id"], $sp_id[$i]));
      }
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    }
}

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: ./');
}
